package br.com.silvamap.yohomap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

public class MensagemListActivity extends ListActivity implements OnItemClickListener {

	private List<Map<String, Object>> mensagens;
	private YoHOSQLiteOpenHelper database;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.opcoes_mensagens, menu);
		return true;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		database = new YoHOSQLiteOpenHelper(this);
		String[] de = {"statusMensagem", "contato", "date", "endereco"};
		int[] para = {R.id.statusMensagem, R.id.contato, R.id.data_mensagem, R.id.endereco_mensagem};
		
		setListAdapter(new SimpleAdapter(this, listarMensagens(), R.layout.lista_mensagem, de, para));
		ListView listView = getListView();
		listView.setOnItemClickListener(this);
	}
	
	@Override
	protected void onDestroy() {
		database.close();
		super.onDestroy();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		System.out.println("");
	}
	
	public List<Map<String, Object>> listarMensagens() {
		mensagens = new ArrayList<Map<String,Object>>();
		
		List<Mensagem> listaMensagem = new ArrayList<Mensagem>();
		YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
		listaMensagem = db.recuperarTodas();
		
		for (Mensagem msg : listaMensagem) {
			Contato contato = new Contato();
			Map<String,Object> item = new HashMap<String, Object>();
			item.put("statusMensagem", retornaImagenStatus(msg.getStatus()));
			String numero = "";
			if(msg.getStatus().equals("recebida") || msg.getStatus().equals("lida")){
				numero = msg.getRemetente();
			} else {
				numero = msg.getDestinatario();
			}
			contato = database.recuperarContatoPorNumero(numero);
			if(contato != null) {
				item.put("contato", contato.getNome());
			} else {
				item.put("contato", numero);
			}
			item.put("date", msg.getDataFormatada());
			item.put("endereco", msg.getEndereco());
			mensagens.add(item);
		}
		
		return mensagens;
	}
	
	
	public int retornaImagenStatus(String status) {
		if(status != null) {
			if(status.equals("enviado")) {
				return R.drawable.ic_amarelo;
			} else if(status.equals("encontrado")) {
				return R.drawable.ic_verde;
			} else if(status.equals("recebida")) {
				return R.drawable.ic_vermelho;
			} else if(status.equals("lida")) {
				return R.drawable.ic_azul;
			}
		} 
		return 0;
	}
	
	public void apagarMensagens(MenuItem item) {
		YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
		db.deletarTodasMensagens();
		startActivity(new Intent(this, MainActivity.class));
	}

}
