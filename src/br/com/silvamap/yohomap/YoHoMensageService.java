package br.com.silvamap.yohomap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

public class YoHoMensageService extends Service {

	YoHOSQLiteOpenHelper db;

	public static List<Mensagem> mensagensParaSerEnviadas = new ArrayList<Mensagem>();
	public static List<Mensagem> mensagensParaSerNotificadas = new ArrayList<Mensagem>();
	public static Contato EU;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		db = new YoHOSQLiteOpenHelper(this);
		while (EU == null) {
			EU = db.recuperarMeuCadastro();
		}
		new RecuperaMesagens().execute();
	}

	public class RecuperaMesagens extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... args) {
			while (true) {
				try {
					Thread.sleep(1500);
					recuperaMensagens();
					enviarMensagens();
					recuperaMensagensEncontradas();
					notificarEncontrouMensagem();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

	}

	public void recuperaMensagens() {
		String url = "http://192.168.43.83/silvamapweb/webService.php?metodo=receberYoho&destinatario=" + EU.getNumero();
		HttpGet httpGet = new HttpGet(url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			HttpResponse getResponse = httpClient.execute(httpGet);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				HttpEntity httResponseEntity = getResponse.getEntity();
				InputStream httpResponseStream = httResponseEntity.getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponseStream, "iso-8859-1"));
				StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					builder.append(line + "\n");
				}
				if(builder.toString().startsWith("{\"mensagens\":")) {
					JSONObject obj = new JSONObject(builder.toString());
					JSONArray mensagens = obj.getJSONArray("mensagens");
					for (int i = 0; i < mensagens.length(); i++) {
						JSONObject msqJson = mensagens.getJSONObject(i);
						Mensagem mensagem = new Mensagem();
						mensagem.setId(msqJson.getInt(Mensagem.ID));
						mensagem.setRemetente(msqJson.getString(Mensagem.REMETENTE));
						mensagem.setDestinatario(msqJson.getString(Mensagem.DESTINATARAIO));
						mensagem.setTexto(msqJson.getString(Mensagem.TEXTO));
						mensagem.setStatus("recebida");
						mensagem.setLat(msqJson.getDouble(Mensagem.LAT));
						mensagem.setLon(msqJson.getDouble(Mensagem.LON));
						try {
							mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(msqJson.getString(Mensagem.DATE)));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						String endereco = recuperaEndereco(mensagem.getLat(), mensagem.getLon());
						mensagem.setEndereco(endereco);
						db.insert(mensagem);
						Contato contato = new Contato();
						String numero = "";
						if (mensagem.getStatus().equals("recebida") || mensagem.getStatus().equals("lida")) {
							numero = mensagem.getRemetente();
						} else {
							numero = mensagem.getDestinatario();
						}
						contato = db.recuperarContatoPorNumero(numero);
						if(contato != null) {
							criarNotificacao(mensagem, contato.getNome(), "postou uma nova mensagem!");
						} else {
							criarNotificacao(mensagem, numero, "postou uma nova mensagem!");
						}
					}
				}
				httpResponseStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void criarNotificacao(Mensagem mensagem, String contato, String texto) {
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher).setTicker(contato + " " + texto)
				.setContentText(contato + " " + texto).setAutoCancel(true);
		Intent intent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
		notificationBuilder.setContentIntent(pendingIntent);
		Notification notification = notificationBuilder.build();
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(mensagem.getId(), notification);
	}

	public void enviarMensagens() {
		for (Mensagem mensagem : mensagensParaSerEnviadas) {
			if (mensagem != null) {
				String url = "http://192.168.43.83/silvamapweb/webService.php?metodo=enviarYoho&" + "remetente=" + mensagem.getRemetente() + "&destinatario=" + mensagem.getDestinatario() + "&mensagem="
						+ mensagem.getTexto() + "&lat=" + String.valueOf(mensagem.getLat()) + "&lon="
						+ String.valueOf(mensagem.getLon() + "&date=" + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(mensagem.getDate()));
				HttpGet httpGet = new HttpGet(url.replace(" ", "%20"));
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpResponse getResponse;
				try {
					getResponse = httpClient.execute(httpGet);
					final int statusCode = getResponse.getStatusLine().getStatusCode();
					if (statusCode == 200) {
						HttpEntity httResponseEntity = getResponse.getEntity();
						InputStream httpResponseStream = httResponseEntity.getContent();
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponseStream, "iso-8859-1"));
						StringBuilder builder = new StringBuilder();
						String line = null;
						while ((line = bufferedReader.readLine()) != null) {
							builder.append(line + "\n");
						}
						if(builder.toString().startsWith("{\"id\":")) {
							JSONObject obj = new JSONObject(builder.toString());
							mensagem.setId(obj.getInt(Mensagem.ID));
							mensagem.setRemetente(obj.getString(Mensagem.REMETENTE));
							mensagem.setDestinatario(obj.getString(Mensagem.DESTINATARAIO));
							mensagem.setTexto(obj.getString(Mensagem.TEXTO));
							mensagem.setStatus(obj.getString(Mensagem.STATUS));
							mensagem.setLat(obj.getDouble(Mensagem.LAT));
							mensagem.setLon(obj.getDouble(Mensagem.LON));
							try {
								mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(obj.getString(Mensagem.DATE)));
							} catch (ParseException e) {
								e.printStackTrace();
							}
							String endereco = recuperaEndereco(mensagem.getLat(), mensagem.getLon());
							mensagem.setEndereco(endereco);
							YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
							db.insert(mensagem);
							mensagensParaSerEnviadas.remove(mensagem);
						}
						httpResponseStream.close();
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void recuperaMensagensEncontradas() {
		String url = "http://192.168.43.83/silvamapweb/webService.php?metodo=recuperarMensagensEncontradas&remetente=" + EU.getNumero();
		HttpGet httpGet = new HttpGet(url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			HttpResponse getResponse = httpClient.execute(httpGet);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				HttpEntity httResponseEntity = getResponse.getEntity();
				InputStream httpResponseStream = httResponseEntity.getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponseStream, "iso-8859-1"));
				StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					builder.append(line + "\n");
				}
				if(builder.toString().startsWith("{\"mensagens\":")) {
					JSONObject obj = new JSONObject(builder.toString());
					JSONArray mensagens = obj.getJSONArray("mensagens");
					for (int i = 0; i < mensagens.length(); i++) {
						JSONObject msqJson = mensagens.getJSONObject(i);
						Mensagem mensagem = new Mensagem();
						mensagem.setId(msqJson.getInt(Mensagem.ID));
						mensagem.setRemetente(msqJson.getString(Mensagem.REMETENTE));
						mensagem.setDestinatario(msqJson.getString(Mensagem.DESTINATARAIO));
						mensagem.setTexto(msqJson.getString(Mensagem.TEXTO));
						mensagem.setStatus(msqJson.getString(Mensagem.STATUS));
						mensagem.setLat(msqJson.getDouble(Mensagem.LAT));
						mensagem.setLon(msqJson.getDouble(Mensagem.LON));
						try {
							mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(msqJson.getString(Mensagem.DATE)));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						db.updateMensagemParaEncontrada(mensagem.getId());
						Contato contato = db.recuperarContatoPorNumero(mensagem.getDestinatario());
						if(contato != null) {
							criarNotificacao(mensagem, contato.getNome(), "econtrou sua mensagem!");
						} else {
							criarNotificacao(mensagem, mensagem.getDestinatario(), "econtrou sua mensagem!");
						}
					}
				}
				httpResponseStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void notificarEncontrouMensagem() {
		for (Mensagem mensagem : mensagensParaSerNotificadas) {
			if (mensagem != null) {
				String url = "http://192.168.43.83/silvamapweb/webService.php?metodo=atualizarMensagemParaEncontrada&id=" + mensagem.getId();
				HttpGet httpGet = new HttpGet(url);
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpResponse getResponse;
				try {
					getResponse = httpClient.execute(httpGet);
					final int statusCode = getResponse.getStatusLine().getStatusCode();
					if (statusCode == 200) {
						HttpEntity httResponseEntity = getResponse.getEntity();
						InputStream httpResponseStream = httResponseEntity.getContent();
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponseStream, "iso-8859-1"));
						StringBuilder builder = new StringBuilder();
						String line = null;
						while ((line = bufferedReader.readLine()) != null) {
							builder.append(line);
						}
						if (builder.toString().equals("atualizado")) {
							mensagensParaSerNotificadas.remove(mensagem);
						}
						httpResponseStream.close();
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String recuperaEndereco(double lat, double lon) {
		String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true";
		HttpGet httpGet = new HttpGet(url);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpResponse getResponse;
		try {
			getResponse = httpClient.execute(httpGet);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				HttpEntity httResponseEntity = getResponse.getEntity();
				InputStream httpResponseStream = httResponseEntity.getContent();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponseStream, "UTF-8"));
				StringBuilder builder = new StringBuilder();
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					builder.append(line + "\n");
				}
				JSONObject obj = new JSONObject(builder.toString());
				if (obj != null) {
					JSONArray results = obj.getJSONArray("results");
					if (results.length() > 0) {
						JSONObject primeroResultado = results.getJSONObject(0);
						return primeroResultado.getString("formatted_address");
					}
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void onDestroy() {
		db.close();
		super.onDestroy();
	}

}
