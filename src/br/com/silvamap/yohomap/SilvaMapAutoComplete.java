package br.com.silvamap.yohomap;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class SilvaMapAutoComplete extends AutoCompleteTextView {

	public SilvaMapAutoComplete(Context context) {
		super(context);
	}

	public SilvaMapAutoComplete(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SilvaMapAutoComplete(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean enoughToFilter() {
		return true;
	}
}
