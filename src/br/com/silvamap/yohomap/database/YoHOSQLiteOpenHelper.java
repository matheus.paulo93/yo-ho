package br.com.silvamap.yohomap.database;

import static br.com.silvamap.yohomap.entidade.Contato.ID_CONTATO;
import static br.com.silvamap.yohomap.entidade.Contato.NOME;
import static br.com.silvamap.yohomap.entidade.Contato.NUMERO;
import static br.com.silvamap.yohomap.entidade.Contato.SOUEU;
import static br.com.silvamap.yohomap.entidade.Contato.TABLE_NAME_CONTATO;
import static br.com.silvamap.yohomap.entidade.Mensagem.COLUMNS;
import static br.com.silvamap.yohomap.entidade.Mensagem.DATE;
import static br.com.silvamap.yohomap.entidade.Mensagem.DESTINATARAIO;
import static br.com.silvamap.yohomap.entidade.Mensagem.ID;
import static br.com.silvamap.yohomap.entidade.Mensagem.LAT;
import static br.com.silvamap.yohomap.entidade.Mensagem.LON;
import static br.com.silvamap.yohomap.entidade.Mensagem.REMETENTE;
import static br.com.silvamap.yohomap.entidade.Mensagem.STATUS;
import static br.com.silvamap.yohomap.entidade.Mensagem.TABLE_NAME;
import static br.com.silvamap.yohomap.entidade.Mensagem.TEXTO;
import static br.com.silvamap.yohomap.entidade.Mensagem.ENDERECO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

public class YoHOSQLiteOpenHelper extends SQLiteOpenHelper {

	public YoHOSQLiteOpenHelper(Context context) {
		super(context, "marketingMap", null, 3);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table " + TABLE_NAME + "(" 
				+ ID + " integer not null primary key, " 
				+ TEXTO + " varchar(500), " 
				+ LAT + " real, " 
				+ LON + " real," 
				+ REMETENTE + " varchar(10)," 
				+ DESTINATARAIO + " varchar(10),"
				+ STATUS + " varchar(50)," 
				+ DATE + " text,"
				+ ENDERECO + " varchar(255))");

		db.execSQL("create table " + TABLE_NAME_CONTATO + "("
				+ ID_CONTATO + " integer not null primary key autoincrement, "
				+ NOME + " varchar(255),"
				+ NUMERO + " varchar(20),"
				+ SOUEU + " integer)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_CONTATO);
		onCreate(db);
	}

	public void insert(Mensagem mensagem) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(ID, mensagem.getId());
		values.put(TEXTO, mensagem.getTexto());
		values.put(LAT, mensagem.getLat());
		values.put(LON, mensagem.getLon());
		values.put(REMETENTE, mensagem.getRemetente());
		values.put(DESTINATARAIO, mensagem.getDestinatario());
		values.put(STATUS, mensagem.getStatus());
		values.put(DATE, new SimpleDateFormat("yyyy-MM-dd HH:mm").format(mensagem.getDate()));
		values.put(ENDERECO, mensagem.getEndereco());
		db.insert(TABLE_NAME, null, values);
		db.close();
	}
	
	public void insert(Contato contato) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NOME, contato.getNome());
		values.put(NUMERO, contato.getNumero());
		if (contato.getSouEu() != null) {
			values.put(SOUEU, contato.getSouEu());
		}
		db.insert(TABLE_NAME_CONTATO, null, values);
		db.close();
	}

	public List<Mensagem> recuperarTodas() {
		List<Mensagem> listaMensagem = new ArrayList<Mensagem>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, COLUMNS, null, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			do {
				Mensagem mensagem = new Mensagem();
				mensagem.setId(cursor.getInt(0));
				mensagem.setTexto(cursor.getString(1));
				mensagem.setLat(cursor.getDouble(2));
				mensagem.setLon(cursor.getDouble(3));
				mensagem.setRemetente(cursor.getString(4));
				mensagem.setDestinatario(cursor.getString(5));
				mensagem.setStatus(cursor.getString(6));
				try {
					mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(cursor.getString(7)));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				mensagem.setEndereco(cursor.getString(8));
				listaMensagem.add(mensagem);
			} while (cursor.moveToNext());
		}
		return listaMensagem;
	}

	public Mensagem recuperarEnviada() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from mensagens where status = 'enviado' limit 1", null);
		if (cursor.moveToFirst()) {
			do {
				Mensagem mensagem = new Mensagem();
				mensagem.setId(cursor.getInt(0));
				mensagem.setTexto(cursor.getString(1));
				mensagem.setLat(cursor.getDouble(2));
				mensagem.setLon(cursor.getDouble(3));
				mensagem.setRemetente(cursor.getString(4));
				mensagem.setDestinatario(cursor.getString(5));
				mensagem.setStatus(cursor.getString(6));
				try {
					mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(cursor.getString(7)));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				mensagem.setEndereco(cursor.getString(8));
				return mensagem;
			} while (cursor.moveToNext());
		}
		return null;
	}

	public Mensagem recuperarMensagemPorLatLon(Mensagem mensagem) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from mensagens where lat = " + mensagem.getLat() + " and lon = " + mensagem.getLon() + " limit 1", null);
		if (cursor.moveToFirst()) {
			do {
				mensagem = new Mensagem();
				mensagem.setId(cursor.getInt(0));
				mensagem.setTexto(cursor.getString(1));
				mensagem.setLat(cursor.getDouble(2));
				mensagem.setLon(cursor.getDouble(3));
				mensagem.setRemetente(cursor.getString(4));
				mensagem.setDestinatario(cursor.getString(5));
				mensagem.setStatus(cursor.getString(6));
				try {
					mensagem.setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(cursor.getString(7)));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				mensagem.setEndereco(cursor.getString(8));
				return mensagem;
			} while (cursor.moveToNext());
		}
		return null;
	}

	public void deletarTodasMensagens() {
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_NAME, null, null);
		db.close();
	}
	
	public void deletarMensagem(Integer id) {
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_NAME, ID + "=" + id, null);
		db.close();
	}

	public void updateMensagemParaEncontrada(Integer id) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(STATUS, "encontrado");
		db.update(TABLE_NAME, values, ID + " = " + id, null);
		db.close();
	}

	public void updateMensagemParaLida(Integer id) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(STATUS, "lida");
		db.update(TABLE_NAME, values, ID + " = ?", new String[] { id.toString() });
		db.close();
	}
	
	public List<Contato> recuperarTodosContatos() {
		List<Contato> listaRetorno = new ArrayList<Contato>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from contato where soueu = 0", null);
		if (cursor.moveToFirst()) {
			do {
				Contato contato = new Contato();
				contato.setId(cursor.getInt(0));
				contato.setNome(cursor.getString(1));
				contato.setNumero(cursor.getString(2));
				contato.setSouEu(cursor.getInt(3));
				listaRetorno.add(contato);
			} while (cursor.moveToNext());
		}
		return listaRetorno;
	}
	
	public Contato recuperarMeuCadastro() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from contato where soueu = 1", null);
		if (cursor.moveToFirst()) {
			do {
				Contato contato = new Contato();
				contato.setId(cursor.getInt(0));
				contato.setNome(cursor.getString(1));
				contato.setNumero(cursor.getString(2));
				contato.setSouEu(cursor.getInt(3));
				return contato;
			} while (cursor.moveToNext());
		}
		return null;
	}
	
	public Contato recuperarContatoPorNumero(String numero){
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select * from contato where numero = '" + numero.trim() + "'", null);
		if (cursor.moveToFirst()) {
			do {
				Contato contato = new Contato();
				contato.setId(cursor.getInt(0));
				contato.setNome(cursor.getString(1));
				contato.setNumero(cursor.getString(2));
				contato.setSouEu(cursor.getInt(3));
				return contato;
			} while (cursor.moveToNext());
		}
		return null;
	}

}
