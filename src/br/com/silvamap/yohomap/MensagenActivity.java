package br.com.silvamap.yohomap;

import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

@SuppressLint("NewApi")
public class MensagenActivity extends Activity {

	private ImageView imageStatusMensagem;
	private TextView contatoMensagem;
	private TextView mensagemTextView;
	private TextView dataMensagem;
	private Mensagem mensagem;
	private YoHOSQLiteOpenHelper database;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		database = new YoHOSQLiteOpenHelper(this);
		super.onCreate(savedInstanceState);
		Bundle bundle = getIntent().getExtras();
		boolean encontrado = bundle.getBoolean("encontrado");
		mensagem = (Mensagem) bundle.getSerializable("mensagem");
		if (encontrado) {
			setContentView(R.layout.activity_mensagem_encontrada);
			imageStatusMensagem = (ImageView) findViewById(R.id.imagem_status_mensagem_encontrada);
			contatoMensagem = (TextView) findViewById(R.id.contato_mensagem_encontrada);
			mensagemTextView = (TextView) findViewById(R.id.mensagem_encontrada);
			dataMensagem = (TextView) findViewById(R.id.id_data_mensagem_encontrada);
			mensagemTextView.setText(mensagem.getTexto());
			imageStatusMensagem.setImageResource(retornaImagenStatus(mensagem.getStatus()));
		} else {
			setContentView(R.layout.activity_mensagen_nao_encontrada);
			imageStatusMensagem = (ImageView) findViewById(R.id.imagem_status_mensagem_nao_encontrada);
			contatoMensagem = (TextView) findViewById(R.id.contato_mensagem_nao_encontrada);
			dataMensagem = (TextView) findViewById(R.id.id_data_mensagem_nao_encontrada);
			imageStatusMensagem.setImageResource(retornaImagenStatus(mensagem.getStatus()));
		}
		Contato contato = new Contato();
		String numero = "";
		if (mensagem.getStatus().equals("recebida") || mensagem.getStatus().equals("lida")) {
			numero = mensagem.getRemetente();
		} else {
			numero = mensagem.getDestinatario();
		}
		contato = database.recuperarContatoPorNumero(numero);
		if(contato != null) {
			contatoMensagem.setText(contato.getNome());
		} else {
			contatoMensagem.setText(numero);
		}
		dataMensagem.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(mensagem.getDate()));
	}

	public int retornaImagenStatus(String status) {
		int retorno = 0;
		if ("enviado".equals(status)) {
			retorno = R.drawable.ic_amarelo;
		} else if ("encontrado".equals(status)) {
			retorno = R.drawable.ic_verde;
		} else if ("recebida".equals(status)) {
			retorno = R.drawable.ic_vermelho;
		} else if ("lida".equals(status)) {
			retorno = R.drawable.ic_azul;
		}
		return retorno;
	}

	@Override
	public void onBackPressed() {
		if (getIntent().getExtras().getBoolean("encontrouAgora")) {
			startActivity(new Intent(this, MainActivity.class));
		}
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		database.close();
		super.onDestroy();
	}

	public void apagarMensagem(View view) {
		if (mensagem != null) {
			if (database != null) {
				database.deletarMensagem(mensagem.getId());
				startActivity(new Intent(this, MainActivity.class));
			}
		}
	}

	public void responderMensagem(View view) {

	}
}
