package br.com.silvamap.yohomap.entidade;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Mensagem implements Serializable {

	private static final long serialVersionUID = 1L;

	// Nome da tabela.
	public final static String TABLE_NAME = "mensagem";

	// Colunas
	public final static String ID = "id";
	public final static String TEXTO = "texto";
	public final static String LAT = "lat";
	public final static String LON = "lon";
	public final static String REMETENTE = "remetente";
	public final static String DESTINATARAIO = "destinatario";
	public final static String STATUS = "status";
	public final static String DATE = "date";
	public final static String ENDERECO = "endereco";
	public final static String[] COLUMNS = { ID, TEXTO, LAT, LON, REMETENTE, DESTINATARAIO, STATUS, DATE, ENDERECO};

	private Integer id;
	private String texto;
	private double lat;
	private double lon;
	private String remetente;
	private String destinatario;
	private String status;
	private Date date;
	private String endereco;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getRemetente() {
		return remetente;
	}

	public void setRemetente(String remetente) {
		this.remetente = remetente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getDataFormatada(){
		return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(getDate());
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

}
