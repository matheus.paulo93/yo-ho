package br.com.silvamap.yohomap.entidade;

import java.io.Serializable;

public class Contato implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Nome da tabela.
	 */
	public final static String TABLE_NAME_CONTATO = "contato";

	/**
	 * Colunas
	 */
	public final static String ID_CONTATO = "id";
	public final static String NOME = "nome";
	public final static String NUMERO = "numero";
	public final static String SOUEU = "soueu";
	public final static String[] COLUMNS_CONTATO = { ID_CONTATO, NOME, NUMERO };

	private Integer id;
	private String nome;
	private String numero;
	private Integer souEu;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome.trim();
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero.trim();
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getSouEu() {
		return souEu;
	}

	public void setSouEu(Integer souEu) {
		this.souEu = souEu;
	}
	
	@Override
	public String toString() {
		return getNome();
	}

}
