package br.com.silvamap.yohomap.receiver;

import br.com.silvamap.yohomap.YoHoMensageService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartupReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent in = new Intent(context, YoHoMensageService.class);
		context.startService(in);
	}

}
