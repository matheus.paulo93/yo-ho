package br.com.silvamap.yohomap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

@SuppressLint("NewApi")
public class MainActivity extends Activity {

	private GoogleMap googleMap;
	private LocationManager locationManager;
	private Map<String, Mensagem> mapaMarkerMensagem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		YoHOSQLiteOpenHelper dbmain = new YoHOSQLiteOpenHelper(this);
		Contato contato = dbmain.recuperarMeuCadastro();
		if (contato == null) {
			contato = new Contato();
			contato.setNome("Louise");
			contato.setNumero("553196072721");
			contato.setSouEu(0);
			dbmain.insert(contato);
			contato = new Contato();
			contato.setNome("Matheus Paulo");
			contato.setNumero("553193225875");
			contato.setSouEu(0);
			dbmain.insert(contato);
			contato = new Contato();
			contato.setNome("Luan");
			contato.setNumero("553191648870");
			contato.setSouEu(0);
			dbmain.insert(contato);
			startActivity(new Intent(this, CadastroActivity.class));
		}
		setContentView(R.layout.activity_main);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		preencheMapa();
		meuLugar();
		googleMap.setMyLocationEnabled(true);
		googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				if (marker.isDraggable()) {
					final Mensagem mensagem = new Mensagem();
					mensagem.setLat(marker.getPosition().latitude);
					mensagem.setLon(marker.getPosition().longitude);
					abreTelaNovoYoHo(mensagem);
				} else {
					Intent intent = new Intent(MainActivity.this, MensagenActivity.class);
					Bundle bundle = new Bundle();
					Mensagem mensagem = mapaMarkerMensagem.get(marker.getId());
					bundle.putSerializable("mensagem", mensagem);
					if (!mensagem.getStatus().equals("recebida")) {
						bundle.putBoolean("encontrado", true);
					} else {
						if (estaPertoDaMensagem(marker)) {
							YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(MainActivity.this);
							db.updateMensagemParaLida(mensagem.getId());
							YoHoMensageService.mensagensParaSerNotificadas.add(mensagem);
							bundle.putBoolean("encontrouAgora", true);
							mensagem.setStatus("lida");
							db.close();
							bundle.putBoolean("encontrado", true);
						} else {
							bundle.putBoolean("encontrado", false);
						}
					}
					intent.putExtras(bundle);
					startActivity(intent);
				}

				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void novoYoho(View view) {
		final MarkerOptions markerOptions = new MarkerOptions();
		googleMap.addMarker(markerOptions.position(new LatLng(getLocation().getLatitude(), getLocation().getLongitude())).draggable(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.marc_roxo)));
	}

	public void abreTelaNovoYoHo(Mensagem mensagem) {
		Intent intent = new Intent(this, NovaMensagemActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("mensagem", mensagem);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	public void listarMensagens(View view) {
		startActivity(new Intent(this, MensagemListActivity.class));
	}

	public void meuLugar() {
		Location location = getLocation();
		if (location != null) {
			googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
			googleMap.animateCamera(CameraUpdateFactory.zoomTo(6), 2000, null);
		}
	}

	public Location getLocation() {
		Location location = null;
		if (locationManager != null) {
			List<String> listProviders = locationManager.getAllProviders();
			for (String provider : listProviders) {
				location = locationManager.getLastKnownLocation(provider);
				if (location != null) {
					return location;
				}
			}
		}
		return location;
	}

	public void preencheMapa() {
		mapaMarkerMensagem = new HashMap<String, Mensagem>();
		List<Mensagem> listaMensaegm = new YoHOSQLiteOpenHelper(this).recuperarTodas();
		for (Mensagem mensagem : listaMensaegm) {
			MarkerOptions markerOptions = new MarkerOptions();
			Marker marker = googleMap.addMarker(markerOptions.position(new LatLng(mensagem.getLat(), 
					mensagem.getLon())).
					draggable(false).
					icon(BitmapDescriptorFactory.fromResource(retornaImagenStatus(mensagem.getStatus()))));
			mapaMarkerMensagem.put(marker.getId(), mensagem);
		}
	}

	public int retornaImagenStatus(String status) {
		if (status != null) {
			if (status.equals("enviado")) {
				return R.drawable.marc_amarelo;
			} else if (status.equals("encontrado")) {
				return R.drawable.marc_verde;
			} else if (status.equals("recebida")) {
				return R.drawable.marc_vermelho;
			} else if (status.equals("lida")) {
				return R.drawable.marc_azul;
			}
		}
		return 0;
	}

	public boolean estaPertoDaMensagem(Marker marker) {
		Location location = getLocation();
		if (location != null) {
			LatLng latLngMarker = marker.getPosition();
			LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

			Long latMarker = Long.parseLong(Double.valueOf(latLngMarker.latitude).toString().replace(".", "").substring(0, 8));
			Long lngMarker = Long.parseLong(Double.valueOf(latLngMarker.longitude).toString().replace(".", "").substring(0, 8));
			Long lat = Long.parseLong(Double.valueOf(latLng.latitude).toString().replace(".", "").substring(0, 8));
			Long lng = Long.parseLong(Double.valueOf(latLng.longitude).toString().replace(".", "").substring(0, 8));

			if (latMarker > lat - 50 && latMarker < lat + 50 && lngMarker > lng - 50 && lngMarker < lng + 50) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}
	
	public void abrirTelaSobre(MenuItem item) {
		startActivity(new Intent(this, SobreActivity.class));
	}
	
	public void recarregarMapa(MenuItem item) {
		recreate();
	}

}
