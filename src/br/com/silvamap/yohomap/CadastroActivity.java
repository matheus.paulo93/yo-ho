package br.com.silvamap.yohomap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;

public class CadastroActivity extends Activity {

	private EditText nomeEditText;
	private EditText numeroEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadastro);
		nomeEditText = (EditText) findViewById(R.id.nome_edit_text);
		numeroEditText = (EditText) findViewById(R.id.numero_edit_text);
	}

	public void cadastrar(View v) {
		YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
		Contato contato = new Contato();
		contato.setNome(nomeEditText.getText().toString());
		contato.setNumero(numeroEditText.getText().toString());
		contato.setSouEu(1);
		db.insert(contato);
		db.close();
		startService(new Intent(this, YoHoMensageService.class));
		startActivity(new Intent(this, MainActivity.class));
	}
}
