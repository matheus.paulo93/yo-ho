package br.com.silvamap.yohomap;

import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Toast;
import br.com.silvamap.yohomap.database.YoHOSQLiteOpenHelper;
import br.com.silvamap.yohomap.entidade.Contato;
import br.com.silvamap.yohomap.entidade.Mensagem;

public class NovaMensagemActivity extends Activity {

	private Mensagem mensagem;
	private AutoCompleteTextView contatoEditText;
	private EditText mensagemEditiText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nova_mensagem);
		contatoEditText = (AutoCompleteTextView) findViewById(R.id.autocompletecontatos);
		mensagemEditiText = (EditText) findViewById(R.id.edittexmensagem);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			mensagem = (Mensagem) bundle.get("mensagem");
		}
		YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
		List<Contato> listaContatos = db.recuperarTodosContatos();
		ArrayAdapter<Contato> adapter = new ArrayAdapter<Contato>(this, android.R.layout.simple_dropdown_item_1line, listaContatos);
		contatoEditText.setAdapter(adapter);
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.nova_mensagem, menu);
		return true;
	}

	public void clickEnviarMensagem(View view) {
		if (mensagem != null) {
			if(!(contatoEditText.getText().toString().equals(""))){
				YoHOSQLiteOpenHelper db = new YoHOSQLiteOpenHelper(this);
				Contato contato = db.recuperarMeuCadastro();
				if(contato != null) {
					mensagem.setRemetente(contato.getNumero());
				}
				if(contatoEditText.getAdapter().getCount() > 0) {
					mensagem.setDestinatario(((Contato) contatoEditText.getAdapter().getItem(0)).getNumero());
				} else {
					mensagem.setDestinatario(contatoEditText.getText().toString());
				}
				mensagem.setTexto(mensagemEditiText.getText().toString());
				mensagem.setDate(new Date());
				YoHoMensageService.mensagensParaSerEnviadas.add(mensagem);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Toast.makeText(this, "Sua mensagem ser� enviada, regarregue o mapa se n�o conseguir vizualiza-la", Toast.LENGTH_LONG).show();
				startActivity(new Intent(this, MainActivity.class));
			} else {
				Toast.makeText(this, "Campo destinat�rio vazio!", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public void alterarLocal(View view) {
		finish();
	}

}
